<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <title>Tambah Data Baru</title>
    <style>
        body{
            background-color: beige;
        }
    </style>
</head>
<body>
    <div class="container mt-5">
        <h1 class="text-center">Form Tambah Data</h1>
        <form action="/data-baru" method="POST">
            @csrf
            <div class="mb-3">
                <label for="nim" class="form-label fw-bold">NIM</label>
                <input type="number" name="nim" id="" class="form-control">
            </div>
            <div class="mb-3">
                <label for="nama" class="form-label fw-bold">Nama</label>
                <input type="text" name="nama" id="" class="form-control">
            </div>
            <div class="mb-3">
                <label for="prodi" class="form-label fw-bold">Prodi</label>
                <input type="text" name="prodi" id="" class="form-control">
            </div>
            <div class="mb-3">
                <label for="fakultas" class="form-label fw-bold">Fakultas</label>
                <input type="text" name="fakultas" id="" class="form-control">
            </div>
            <div class="mb-3">
                <label for="fakultas" class="form-label fw-bold">Jenis Kelamin</label>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="jenis_kelamin" id="flexRadioDefault1" value="perempuan">
                    <label class="form-check-label" for="flexRadioDefault1">
                        Perempuan
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="jenis_kelamin" id="flexRadioDefault1" value="laki-laki">
                    <label class="form-check-label" for="flexRadioDefault1">
                        Laki-laki
                    </label>
                </div>
            </div>
            <button type="submit" class="btn btn-success">Tambah Data</button>
        </form>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

</body>
</html>