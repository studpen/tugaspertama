<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <title>View Data Mahasiswa</title>
    <style>
        body{
            background-color: beige;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1 class="text-center mt-5">Data Mahasiswa</h1>
    <a href="/new-mhs" class="btn btn-primary mb-3"> Tambah Data Baru </a>
    <table class="table table-dark table-striped">
        <thead>
            <th> NIM </th>
            <th> Nama </th>
            <th> Prodi </th>
            <th> Fakultas </th>
            <th> Jenis Kelamim </th>
        </thead>
        <tbody>
            @foreach ($data as $item)
                <tr>
                    <td> {{ $item->nim }} </td>
                    <td> {{ $item->nama }} </td>
                    <td> {{ $item->prodi }} </td>
                    <td> {{ $item->fakultas }} </td>
                    <td> {{ $item->jenis_kelamin }} </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
</body>
</html>